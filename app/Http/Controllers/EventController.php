<?php

namespace App\Http\Controllers;
Use DB;
use Illuminate\Http\Request;
use App\Models\Event;
use Illuminate\Support\Facades\Redirect;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        //$events= Event::Latest('id')->get();
        return view('events.index',compact('events'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        print_r($request->input());
        $events = new Event;
        $events->ename= $request->ename;
        $events->descp= $request->descp;
        echo $events->save();
        return redirect('/events')->with('success', 'New event created!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $events =Event::find($id);
        return view('events.edit',compact('events'));

       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
   */
 
    public function update(Request $request, $id)
    {
        $request->validate([
            'ename'=>'required',
            'descp'=>'required'
        ]);


        $events = Event::find($id);
        $events->ename =  $request->get('ename');
        $events->descp = $request->get('descp');
    
        $events->save();

        return redirect('/events')->with('success', 'Event updated!');
    }

   

    public function list()
    {
        $events = Event::all();
        //$events= Event::Latest('id')->get();

        return view ('events.list',compact('events'));


        //return view ('events.list');
    }

    public function join($id)
    {
        
        $users =Event::find($id);
        $users->event()->attach($events);

        return 'Success';
        //return view('events.join',compact('events'));

    }

    /**
     * Remove the specified resource from storage.s
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $events = Event::find($id);
        $events->delete();

        return redirect('/events')->with('success', 'Event deleted!');

    }

   

}
