<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Event;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;

class adminHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       //$users = User::Latest('id')->get();
      //  $users = User::all();
       //return view('adminHome',['users' => $users]);

      
        $events=Event::with('users')->get();
        return view('adminHome',compact('events'));
    }

    public function adminHome()
    {
        
        return view('adminHome');
    }

    public function create()
    {
        
        return view('events');

    }
    
    public function show()
    {
       $users = User::Latest('id')->get();
      //  $users = User::all();
       return view('memberlist',['users' => $users]);
    }
    
    public function edit()
    {
       //$users = User::Latest('id')->get();
      //  $users = User::all();
      // return view('editstatus',['users' => $users]);

       $users =Event::find($id);
       return view('editstatus');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'is_admin'=>'required',
          
        ]);


        $users = User::find($id);
        $users->is_admin =  $request->get('is_admin');
       
        $users->save();

        return redirect('/memberlist')->with('success', 'Data updated!');
    }

}