@extends('layouts.admin')

@section('content')

<div class="main">  
<div class="col-sm-12">


            



<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Edit user status</h1>
    @method('PATCH') 
            @csrf
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <td>ID</td>
                                <td>Name</td>
                                <td>Action</td>                            
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <th>{{$user->id}}</th>
            <th>{{$user->name}}</th>
            @if($user->is_admin== '1')
           <th> 
           <div class = "radio">
        <label><input type = "radio" name = "is_admin" value = "$users->id" checked="checked"> Admin</label>
    </div>
           </br>
           <th> 
           <input type="radio" id="0" name="is_admin" value="false">Member</th>
           </br>
           <th>
           <input type="radio" id="null" name="is_admin" value="false">Block</th>
           @elseif($user->is_admin== '0')
           <th> 
           <input type="radio" id="1" name="is_admin" value="true">Admin</th>
           </br>
           <th> 
           <input type="radio" id="0" name="is_admin" value="false" checked="checked">Member</th>
           </br>
           <th> 
           <input type="radio" id="null" name="is_admin" value="false">Block</th>
           @else
           <th> 
           <input type="radio" id="1" name="is_admin" value="true">Admin</th>
           </br>
           <th>
           <input type="radio" id="0" name="is_admin" value="false">Member</th>
           </br>
           <th> 
           <input type="radio" id="null" name="is_admin" value="false" checked="checked">Blocked</th>
        
            </tr>
            @endif
            @endforeach
    </tbody>
  </table>
  </div>
 </div>
 </div>
</div>
  @endsection

  