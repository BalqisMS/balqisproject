@extends('layouts.admin')

@section('content')

<div class="main">  
<div class="col-sm-12">


            



<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Users</h1>
    </br>
   


                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <td>ID</td>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Status</td>
                                <td>Created at</td>
                                <td colspan = 1>Actions</td>                      
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <th>{{$user->id}}</th>
            <th>{{$user->name}}</th>
            <th>{{$user->email}}</th>
            <th>@if($user->is_admin== 1)Admin @elseif($user->is_admin== '0') Member @else Inactive user @endif</th>
            <th>{{$user->created_at}}</th>
            <th>
            <a href="{{ route('editstatus',$user->id)}}" class="btn btn-primary">Edit</a>
            </th>
            </tr>
            @endforeach
    </tbody>
  </table>
  </div>
 </div>
 </div>
</div>
  @endsection