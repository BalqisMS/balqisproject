@extends('layouts.user')
@section('content')
<div class="main">  
<div class="col-sm-12">


            <div class="alert alert-success alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                <strong>Welcome!</strong> You are logged in.

            </div>
            @if(session('message'))
            <div class="alert alert-success">
            {{session('message')}}
            </div>
           @endif

        <h1> Welcome to VVD Website</h1>


            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">


                    <table class="table table-striped">
                        <thead>
                            <tr>
                            <td>ID</td>
                                <td>Name</td>
                                <td>Email</td>
                                
                                
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <th>{{$user->id}}</th>
            <th>{{$user->name}}</th>
            <th>{{$user->email}}</th>
            </tr>
            @endforeach
    </tbody>
  </table>
 </div>
 </div>
           

               
    
</div>
</div>
@endsection