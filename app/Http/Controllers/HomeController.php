<?php

namespace App\Http\Controllers;
use App\Models\Event;
use Illuminate\Http\Request;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       /*
        $events = Event::all();
        return view('home',['events' => $events]);
        */
        $events = Event::all();
        //$events= Event::Latest('id')->get();
        return view('events.list',compact('events'));
        //return view ('home');
    }

    public function adminHome()
    {
        return view('adminHome');
    }

    public function join()
    {
        
        return view('events');

    }

  
}
