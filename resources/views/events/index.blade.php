@extends('layouts.admin')

@section('content')
<div class="main"> 
<div class="col-sm-12">

@if(session()->get('success'))
  <div class="alert alert-success">
    {{ session()->get('success') }}  
  </div>
@endif
</div>
         

<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Events</h1>
    </br>
    <div>
    <a style="margin: 19px;" href="/events/create" class="btn btn-primary">Add Event</a>
    </div>  
    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Description</td>

          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
    @foreach($events as $event)
        <tr>
            <td>{{$event->id}}</td>
            <td>{{$event->ename}}</td>
            <td>{{$event->descp}}</td>
            <td>
                <a href="{{ route('events.edit',$event->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
            <form action="{{ route ('events.destroy', $event->id)}}" method="post">
                  @csrf
                  @method('DELETE')
              <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
       
        @endforeach

  
    </tbody>
  </table>
<div>
</div>
</div> 
@endsection
