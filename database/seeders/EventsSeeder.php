<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Event::class, 10)->create();

        foreach(Event::all() as $events){
    
        $users = \App\Models\User::inRandomOrder()->take(rand(1,3))->pluck('id');
        $events->users()->attach($users);
    }

/*
        $events=[
            [
                'ename'=>'Expert Forum and EU Budget',
                'descp'=>'5 January 2021, 12:00pm, At Beeld en Geluid Den Haag, The Hague',

            ],
            [
                'ename'=>'For Children EU Furure',
                'descp'=>'8 January 2021, 2:00pm, Online',
                
            ],    
            ];
            foreach ($users as $key => $value) {
                User::create($value);
            }
*/

    }
}
