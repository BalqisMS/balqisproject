

@extends('layouts.admin')
@section('content')

<div class="main">
<div class="container1">

    <br/>
<h1>Add New Event</h1>
<br/>
<div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif

<form  action="{{ route('events.store') }}" method="POST">
   @csrf
   
  <div class="form-group">
    <label for="ename">Name:</label>
    <input type="text" class="form-control" name="ename"  placeholder="Enter event name">
    </div> 



<div class="form-group">
  <label for="descp">Description:</label>
  <textarea class="form-control" rows="5" name="descp"></textarea>
</div>
  
  <button type="submit" class="btn btn-primary">Add Event</button>
</form>
</div>
</div>
</html>
@endsection