@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Edit Event</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('events.update', $events->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="ename">Event Name:</label>
                <input type="text" class="form-control" name="ename" value={{ $events->ename }} />
            </div>

            <div class="form-group">
                <label for="descp">Description:</label>
                <textarea class="form-control" rows="5" name="descp" value={{ $events->descp }} /></textarea>
            </div>

           
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection