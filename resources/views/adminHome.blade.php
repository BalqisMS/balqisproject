@extends('layouts.admin')

@section('content')

<div class="main">  
<div class="col-sm-12">

@if(session('message'))
            <div class="alert alert-success alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                <strong>Welcome!</strong> You are logged in as Admin.
                {{session('message')}}
            </div>
           @endif
            

        



           <div class="row">
<div class="col-sm-12">
    <div class="display-3">Dashboard</div>
    </br>
      
    
  <table class="table table-striped">
    <thead>
                            <tr>
                            <td>Event</td>
                                <td>Participants</td>                       
        </tr>
    </thead>
    <tbody>
    @foreach($events as $event)
        <tr>
            <td>{{$event->ename}}</td>
            <td>
            <ul>
            @foreach ($event->users as $user)
            <li>{{$user->name}} {{$user->pivot->created_at}}</li>
            @endforeach
            </ul>
            </td>
            </tr>
            @endforeach
    </tbody>
  </table>

 </div>
 </div>
           

               
    
</div>
</div>

@endsection

