@extends('layouts.user')

@section('content')
<div class="main"> 
<div class="col-sm-12">
<h1> Welcome to VVD Website</h1>
</br>
</br>
</br>
<div class="alert alert-success alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

                <strong>Welcome!</strong> You are logged in.

            </div>

@if(session()->get('success'))
  <div class="alert alert-success">
    {{ session()->get('success') }}  
  </div>
@endif
</div>
         


 
     
    <div class="card">
                <div class="card-header"><b>Event List</b></div>
                <div class="card-body"> 
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Description</td>

          
        </tr>
    </thead>
    <tbody>
    @foreach($events as $event)
        <tr>
            <td>{{$event->id}}</td>
            <td>{{$event->ename}}</td>
            <td>{{$event->descp}}</td>
            <td>
                <a href="{{ route('events.join',$event->id)}}" class="btn btn-primary">Join</a>
            </td>
            
        </tr>
       
        @endforeach

  
    </tbody>
  </table>
<div>
</div>

@endsection