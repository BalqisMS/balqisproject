<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/events/join', function () {
    return view('events.join');
});



Route::resource('events', 'App\Http\Controllers\EventController');
/*
Route::get('/event', function () {
    return view('event');

});
*/
Auth::routes();
Route::get('/events/join/{id}',[App\Http\Controllers\EventController::class, 'join'])->name('events.join');
Route::get('/events/list',[App\Http\Controllers\EventController::class, 'list'])->name('events.list');

Route::patch('/events/{id}',[App\Http\Controllers\EventController::class, 'update'])->name('events.update');

Route::get('/users/join',[App\Http\Controllers\EventController::class, 'join'])->name('users.join');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/home', [App\Http\Controllers\adminHomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
Route::get('admin/home', [App\Http\Controllers\adminHomeController::class, 'index'])->name('admin.home')->middleware('is_admin');
Route::get('/memberlist', [App\Http\Controllers\adminHomeController::class, 'show'])->name('memberlist');
Route::patch('/editstatus/{id}', [App\Http\Controllers\adminHomeController::class, 'edit'])->name('editstatus');
Route::get('/editstatus/{id}', [App\Http\Controllers\adminHomeController::class, 'update'])->name('editstatus');